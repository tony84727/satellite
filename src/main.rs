#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use rocket_contrib::serve::StaticFiles;
use rocket::http;
use std::fs;
use std::path::Path;
use rocket::response::status::NotFound;

extern crate md5;
extern crate base64;

#[get("/")]
fn index() -> &'static str {
    "Hello, world!"
}

#[get("/<path>")]
fn script_version(path: &http::RawStr) -> Result<String,NotFound<String>> {
    let prefix = Path::new("lua");
    let path = path.to_string();

    fs::read(prefix.join(&path).as_os_str()).map_err(|_e| NotFound(format!("{} not found", &path)))
        .map(md5::compute)
        .map(|d|base64::encode(&d.0))
}

fn main() {
    rocket::ignite()
        .mount("/", routes![index])
        .mount("/scripts/content",StaticFiles::from("lua"))
        .mount("/scripts/version", routes![script_version])
        .launch();
}